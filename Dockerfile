FROM php:8.3-rc-fpm-alpine3.19

ARG APP_ENV='prod'

RUN set -xe; \
        apk add --update -t .php-run-deps \
        bash \
        supervisor \
        freetype \
        patch \
        mysql-client \
        icu-libs \
        libbz2 \
        libevent \
        libjpeg-turbo \
        libjpeg-turbo-utils \
        libmcrypt \
        libpng \
        libuuid \
        libwebp \
        libxml2 \
        libxslt \
        libzip \
        git \
        imagemagick-dev \
        libtool \
        ncurses \
        yaml && \
    apk add --update -t .php-build-deps \
        g++ \
        make \
        autoconf \
        libzip-dev \
        icu-dev \
        bzip2-dev \
        freetype-dev \
        libmcrypt-dev \
        jpeg-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        libwebp-dev \
        unixodbc-dev \
        yaml-dev && \
    docker-php-ext-install \
        bcmath \
        bz2 \
        calendar \
        exif \
        ftp \
        intl \
        mysqli \
        opcache \
        pcntl \
        pdo_mysql \
        zip; \
    docker-php-ext-configure gd --with-webp --with-freetype --with-jpeg; \
    NPROC=$(getconf _NPROCESSORS_ONLN); \
    docker-php-ext-install "-j${NPROC}" gd; \
    #  Additional libraries for pdo_sqlsrv and sqlsrv
    curl -O https://download.microsoft.com/download/e/4/e/e4e67866-dffd-428c-aac7-8d28ddafb39b/msodbcsql17_17.6.1.1-1_amd64.apk && \
    apk add --allow-untrusted msodbcsql17_17.6.1.1-1_amd64.apk; \
    curl -O https://download.microsoft.com/download/e/4/e/e4e67866-dffd-428c-aac7-8d28ddafb39b/mssql-tools_17.6.1.1-1_amd64.apk && \
    apk add --allow-untrusted mssql-tools_17.6.1.1-1_amd64.apk && \
    \
    pecl channel-update pecl.php.net && \
    pecl install yaml \
                 # Versions for php 8.1
                 pdo_sqlsrv-5.10.1 \
                 sqlsrv-5.10.1 \
                 redis \
                 mcrypt && \
    docker-php-ext-enable yaml \
                          pdo_sqlsrv \
                          redis \
                          sqlsrv \
                          mcrypt


RUN apk add \
      automake \
      nginx \
      npm && \
    ln -s /usr/local/lib/php/extensions/no-debug-non-zts-20210902/sqlsrv.so /usr/local/lib/php/extensions/no-debug-non-zts-20210902/sqlsrv ; \
    npm install --global yarn && \
    echo "error_log = \"/proc/self/fd/2\"" | tee -a $PHP_INI_DIR/php.ini && \
    wget -qO- https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --2.7 \
